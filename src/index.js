const ethers = require('ethers');

MORALIS_API = "https://speedy-nodes-nyc.moralis.io/fd0d8f36c9c1f2a508254528/polygon/mumbai";
WALLET_ADDRESS = "";
WALLET_PRIVATE_KEY = "";
DERC20_ABI = require('../abis/erc20');
DERC20_CONTRACT_ADDRESS = "0xfe4F5145f6e09952a5ba9e956ED0C25e3Fa4c7F1";

const provider = new ethers.providers.JsonRpcProvider(MORALIS_API);

async function init() {
  const maticBalance = await provider.getBalance(WALLET_ADDRESS);
  
  const tokenContract = new ethers.Contract(DERC20_CONTRACT_ADDRESS, DERC20_ABI, provider);
  
  const tokenBalance = await tokenContract.balanceOf(WALLET_ADDRESS);
  console.log("Showing balance for testnet wallet");
  console.log("MATIC balance: ", ethers.utils.formatEther(maticBalance));  
  console.log("DERC20 balance: ", ethers.utils.formatUnits(tokenBalance)); 
}

function submitHumanInfo() {
  return true;
}

function readHumanInfo() {
  return true;
}

function sendGLODistribution() {
  return true;
}

function humanInfo() {
  let johnDoe = {
    user123: {
      name: 'John Doe Of Denham',
      dob: '10-10-1980',
      nationality: 'NL',
      email: 'john.doe@denham.com',
      tel: '123',
      cob: 'abc',
      pob: 'def',
      country: 'ghi',
      city: 'jkl',
      street: 'mno',
      nin: 'pqr',
      idn: 'stu',
      kyc_date: new Date().toISOString().split('T')[0],
      wallet: '0xD6B2fed043153cC9A4698773b8721237626ecF29',
    }
  };
  return johnDoe;
}



init();


